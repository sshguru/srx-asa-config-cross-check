#!/bin/bash
# srx.sh
# usage: srxcompare.sh <SRX-config> <IP address>

SRX=$1
IPA=$2

mkdir tmpcsrx
cp $SRX tmpcsrx/$SRX
cd tmpcsrx

touch srxcgroups srxcips srxcparents srxcpolicies srxcparents_bak srxcgparents srxcgparents_bak srxcggparents srxcggparents_bak srxcapps srxcapplications PScount srxcpoliciesall

echo $'IP-' >> result-csrx
grep -w "set security address-book global address $IPA" $SRX >> result-csrx
grep -w "set security address-book global address $IPA" $SRX | awk '{ print $7 }' > srxcips
echo $'-IP' >> result-csrx

echo $'GS-' >> result-csrx
echo $'G1-' >> result-csrx
grep -w "set security address-book global address-set" $SRX | grep -w "address $IPA" | awk '{ print $6 }' >> result-csrx
grep -w "set security address-book global address-set" $SRX | grep -w "address $IPA" | awk '{ print $6 }' >> srxcgroups
echo $'-G1' >> result-csrx

echo $'G2-' >> result-csrx
cat srxcgroups | while read group
do
	grep -w "set security address-book global address-set" $SRX | grep -w "address-set $group" | awk '{ print $6 }' | uniq >> srxcparents
done
cat srxcgroups | while read group
do
	sed -i_bak -e "/$group/d" srxcparents
done
cat srxcparents >> result-csrx
echo $'-G2' >> result-csrx

echo $'G3-' >> result-csrx
cat srxcparents | while read parent
do
	grep -w "set security address-book global address-set" $SRX | grep -w "address-set $parent" | awk '{ print $6 }' | uniq >> srxcgparents
done
cat srxcparents | while read parent
do
	sed -i_bak -e "/$parent/d" srxcgparents
done
cat srxcgparents >> result-csrx
echo $'-G3' >> result-csrx

echo $'G4-' >> result-csrx
cat srxcgparents | while read gparent
do
	grep -w "set security address-book global address-set" $SRX | grep -w "address-set $gparent" | awk '{ print $6 }' | uniq >> srxcggparents
done
cat srxcgparents | while read gparent
do
	sed -i_bak -e "/$gparent/d" srxcggparents
done
cat srxcggparents >> result-csrx
echo $'-G4' >> result-csrx
echo $'-GS' >> result-csrx

cat srxcgroups > srxcallgroups
cat srxcparents >> srxcallgroups
cat srxcgparents >> srxcallgroups
cat srxcggparents >> srxcallgroups

grep -w "set security policies global policy" $SRX | grep -w "$IPA" | awk '{ print $6 }' >> srxcpoliciesall
cat srxcips | while read ip
do
	grep -w "set security policies global policy" $SRX | grep -w "$ip" | awk '{ print $6 }' >> srxcpoliciesall
done
cat srxcgroups | while read group
do
	grep -w "set security policies global policy" $SRX | grep -w "$group" | awk '{ print $6 }' >> srxcpoliciesall
done
cat srxcparents | while read parent
do
	grep -w "set security policies global policy" $SRX | grep -w "$parent" | awk '{ print $6 }' >> srxcpoliciesall
done
cat srxcgparents | while read gparent
do
	grep -w "set security policies global policy" $SRX | grep -w "$gparent" | awk '{ print $6 }' >> srxcpoliciesall
done
cat srxcggparents | while read ggparent
do
	grep -w "set security policies global policy" $SRX | grep -w "$ggparent" | awk '{ print $6 }' >> srxcpoliciesall
done

echo "10%"

echo "PS-" >> result-csrx
echo "0" > PScount

cat srxcpoliciesall | sort -r | uniq > srxcpolicies

cat srxcpolicies | while read policy
do
	cat srxcallgroups | while read group
	do
		if grep -w "set security policies global policy $policy match destination-address $group" $SRX >> /dev/null
		then
			p=$(cat PScount)
			p=$((p+1))
			echo $p > PScount
			head=$(grep -w "set security policies global policy $policy" $SRX | head -1 | awk '{ print $6 "-" }')
			echo "PS${p} ${head}" >> result-csrx
			grep -w "set security policies global policy $policy match destination-address $group" $SRX >> result-csrx
			grep -w "set security policies global policy $policy" $SRX | grep -w "match source-address" >> result-csrx
			grep -w "set security policies global policy $policy" $SRX | grep -w "match application" >> result-csrx
			grep -w "set security policies global policy $policy" $SRX | grep -w "match application" | awk '{ print $9 }'>> srxcapps
			grep -w "set security policies global policy $policy" $SRX | head -1 | awk '{ print "-"  $6 }' >> result-csrx
		else 
			if grep -w "set security policies global policy $policy match source-address $group" $SRX >> /dev/null
			then
				p=$(cat PScount)
				p=$((p+1))
				echo $p > PScount
				head=$(grep -w "set security policies global policy $policy" $SRX | head -1 | awk '{ print $6 "-" }')
				echo "PS${p} ${head}" >> result-csrx
				grep -w "set security policies global policy $policy match destination-address $group" $SRX >> result-csrx
				grep -w "set security policies global policy $policy" $SRX | grep -w "match destination-address" >> result-csrx
				grep -w "set security policies global policy $policy" $SRX | grep -w "match application" >> result-csrx
				grep -w "set security policies global policy $policy" $SRX | grep -w "match application" | awk '{ print $9 }'>> srxcapps
				grep -w "set security policies global policy $policy" $SRX | head -1 | awk '{ print "-"  $6 }' >> result-csrx
			fi
		fi
	done
	if grep -w "set security policies global policy $policy match destination-address $IPA" $SRX >> /dev/null
	then
		p=$(cat PScount)
		p=$((p+1))
		echo $p > PScount
		head=$(grep -w "set security policies global policy $policy" $SRX | head -1 | awk '{ print $6 "-" }')
		echo "PS${p} ${head}" >> result-csrx
		grep "set security policies global policy $policy match destination-address $IPA" $SRX >> result-csrx
		grep -w "set security policies global policy $policy" $SRX | grep -w "match source-address" >> result-csrx
		grep -w "set security policies global policy $policy" $SRX | grep -w "match application" >> result-csrx
		grep -w "set security policies global policy $policy" $SRX | grep -w "match application" | awk '{ print $9 }'>> srxcapps
		grep -w "set security policies global policy $policy" $SRX | head -1 | awk '{ print "-"  $6 }' >> result-csrx	
	else
		if grep -w "set security policies global policy $policy match source-address $IPA" $SRX >> /dev/null
		then
			p=$(cat PScount)
			p=$((p+1))
			echo $p > PScount
			head=$(grep -w "set security policies global policy $policy" $SRX | head -1 | awk '{ print $6 "-" }')
			echo "PS${p} ${head}" >> result-csrx
			grep "set security policies global policy $policy match source-address $IPA" $SRX >> result-csrx
			grep -w "set security policies global policy $policy" $SRX | grep -w "match destination-address" >> result-csrx
			grep -w "set security policies global policy $policy" $SRX | grep -w "match application" >> result-csrx
			grep -w "set security policies global policy $policy" $SRX | grep -w "match application" | awk '{ print $9 }'>> srxcapps
			grep -w "set security policies global policy $policy" $SRX | head -1 | awk '{ print "-"  $6 }' >> result-csrx
		fi
	fi
	if grep -w "set security policies global policy $policy match destination-address $(echo $IPA | sed 's|/|-|g')" $SRX >> /dev/null
	then
		p=$(cat PScount)
		p=$((p+1))
		echo $p > PScount
		head=$(grep -w "set security policies global policy $policy" $SRX | head -1 | awk '{ print $6 "-" }')
		echo "PS${p} ${head}" >> result-csrx
		grep -w "set security policies global policy $policy match destination-address $(echo $IPA | sed 's|/|-|g')" $SRX >> result-csrx
		grep -w "set security policies global policy $policy" $SRX | grep -w "match source-address" >> result-csrx
		grep -w "set security policies global policy $policy" $SRX | grep -w "match application" >> result-csrx
		grep -w "set security policies global policy $policy" $SRX | grep -w "match application" | awk '{ print $9 }'>> srxcapps
		grep -w "set security policies global policy $policy" $SRX | head -1 | awk '{ print "-"  $6 }' >> result-csrx	
	else
		if grep -w "set security policies global policy $policy match source-address $(echo $IPA | sed 's|/|-|g')" $SRX >> /dev/null
		then
			p=$(cat PScount)
			p=$((p+1))
			echo $p > PScount
			head=$(grep -w "set security policies global policy $policy" $SRX | head -1 | awk '{ print $6 "-" }')
			echo "PS${p} ${head}" >> result-csrx
			grep -w "set security policies global policy $policy match source-address $(echo $IPA | sed 's|/|-|g')" $SRX >> result-csrx
			grep -w "set security policies global policy $policy" $SRX | grep -w "match destination-address" >> result-csrx
			grep -w "set security policies global policy $policy" $SRX | grep -w "match application" >> result-csrx
			grep -w "set security policies global policy $policy" $SRX | grep -w "match application" | awk '{ print $9 }'>> srxcapps
			grep -w "set security policies global policy $policy" $SRX | head -1 | awk '{ print "-"  $6 }' >> result-csrx
		fi
	fi
done
echo "-PS" >> result-csrx

echo "20%"

echo "apps-" >> result-csrx
sort srxcapps | uniq > srxcapplications
cat srxcapplications | while read app
do
	echo "${app}-" >> result-csrx
	grep -F "set applications application $app " $SRX >> result-csrx
	grep -F "set applications application-set $app " $SRX >> result-csrx
	echo "-${app}" >> result-csrx
done
echo "-apps" >> result-csrx

cd ..
mv "./tmpcsrx/result-csrx" "./result-csrx"