# How to run

chmod +x asacompare.sh
chmod +x srxcompare.sh
chmod +x configaufit.sh
./configaudit.sh <path/to/SRX/config> <path/to/ASA/config> <IP-or-CIDR>

Result will be saved as: audit-result-<IP-or-CIDR>
