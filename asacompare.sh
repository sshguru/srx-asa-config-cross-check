#!/bin/bash
# asa.sh
# usage: asacompare.sh <ASA-config> <IP address>

ASA=$1
IPA=$(echo $2 | sed 's:/.*::')

mkdir tmpcasa
cp $ASA tmpcasa/$ASA
cd tmpcasa

touch asacgroups asacparents asacgparents asacggparents asacrules asacapps asacapplications asacpobjects asactpobjects

echo $'IP-' >> result-casa
grep -w -A 1 "object network $IPA" $ASA >> result-casa
echo $'-IP' >> result-casa

echo $'GS-' >> result-casa
echo $'G1-' >> result-casa
tac $ASA | grep -nw "network-object object $IPA" | cut -d : -f 1 | while read -r num
do
	touch asacgroups
	tac $ASA | tail -n +$num | grep -m 1 "object-group network" | awk '{ print $3 }' >> asacgroups
	tac $ASA | tail -n +$num | grep -m 1 "object-group network" | awk '{ print $3 }' >> result-casa
done
echo $'-G1' >> result-casa

echo $'G2-' >> result-casa
cat asacgroups | while read group
do
	tac $ASA | grep -nw "group-object $group" | cut -d : -f 1 | while read -r num
	do
		touch asacgroups
		tac $ASA | tail -n +$num | grep -m 1 "object-group network" | awk '{ print $3 }' >> asacgroups
		tac $ASA | tail -n +$num | grep -m 1 "object-group network" | awk '{ print $3 }' >> asacparents
		tac $ASA | tail -n +$num | grep -m 1 "object-group network" | awk '{ print $3 }' >> result-casa
	done
done
echo $'-G2' >> result-casa

echo $'G3-' >> result-casa
cat asacparents | while read parent
do
	tac $ASA | grep -nw "group-object $parent" | cut -d : -f 1 | while read -r num
	do
		tac $ASA | tail -n +$num | grep -m 1 "object-group network" | awk '{ print $3 }' >> asacgroups
		tac $ASA | tail -n +$num | grep -m 1 "object-group network" | awk '{ print $3 }' >> asacgparents
		tac $ASA | tail -n +$num | grep -m 1 "object-group network" | awk '{ print $3 }' >> result-casa
	done
done
echo $'-G3' >> result-casa

echo $'G4-' >> result-casa
cat asacgparents | while read gparent
do
	tac $ASA | grep -nw "group-object $gparent" | cut -d : -f 1 | while read -r num
	do
		tac $ASA | tail -n +$num | grep -m 1 "object-group network" | awk '{ print $3 }' >> asacgroups
		tac $ASA | tail -n +$num | grep -m 1 "object-group network" | awk '{ print $3 }' >> asacggparents
		tac $ASA | tail -n +$num | grep -m 1 "object-group network" | awk '{ print $3 }' >> result-casa
	done
done
echo $'-G4' >> result-casa
echo $'-GS' >> result-casa

echo $'RS-' >> result-casa
grep -w "access-list global_in extended permit" $ASA | grep -w $IPA >> result-casa
cat asacgroups | while read group
do
	grep -w "access-list global_in extended" $ASA | grep -w $group >> result-casa
	grep -w "access-list global_in extended" $ASA | grep -w $group >> asacrules
done
echo $'-RS' >> result-casa

echo "40%"

echo $'apps-' >> result-casa
cat asacrules | while read rule
do
	grep -w "access-list global_in extended permit object-group" $ASA | grep -w "$rule" | awk '{ print $6 }' >> asacapps
done
sort asacapps | uniq > asacapplications
cat asacapplications | while read app
do
	echo "${app}-" >> result-casa
	grep -w "object-group service $app" $ASA >> result-casa
	grep -nw "object-group service $app" $ASA | cut -d : -f 1 | while read -r num
	do
		f=0
		while [ $f -lt 1 ]
		do
			num=$((num+1))
			temp="${num}p"
			nextline=$(sed -n $temp $ASA)
			if echo $nextline | grep "object-group service" >> /dev/null
			then
				f=1
			else
				echo $nextline >> result-casa
				if echo $nextline | grep "group-object" >> /dev/null
				then
					echo $nextline | awk '{ print $2 }' >> asactpobjects
				fi
			fi
		done
	done
	echo "-${app}" >> result-casa
done

echo "50%"

sort asactpobjects | uniq > asacpobjects
cat asacpobjects | while read pobject
do
	echo "${pobject}-" >> result-casa
	grep -w "object-group service $pobject" $ASA >> result-casa
	grep -nw "object-group service $pobject" $ASA | cut -d : -f 1 | while read -r num
	do
		f=0
		while [ $f -lt 1 ]
		do
			num=$((num+1))
			temp="${num}p"
			nextline=$(sed -n $temp $ASA)
			if echo $nextline | grep "object-group service" >> /dev/null
			then
				f=1
			else
				echo $nextline >> result-casa
			fi
		done
	done
	echo "-${pobject}" >> result-casa
done
echo $'-apps' >> result-casa

cd ..
mv "./tmpcasa/result-casa" "./result-casa"