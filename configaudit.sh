#!/bin/bash
# audit.sh
# usage: conbfigaudit.sh <SRX-config> <ASA-config> <IP-Address>

folder=$(echo $3 | sed 's|/|-|g')
mkdir .configaudit.$folder
cp $1 .configaudit.$folder/$1
cp $2 .configaudit.$folder/$2
cd .configaudit.$folder

SRXc=$1
ASAc=$2
IPA=$3

../srxcompare.sh $SRXc $IPA
echo "30%"
../asacompare.sh $ASAc $IPA
echo "60%"
SRX=result-csrx
ASA=result-casa

mkdir tmpaudit
cp $SRX tmpaudit/$SRX
cp $ASA tmpaudit/$ASA
cd tmpaudit

if grep -w "set security address-book global address" $SRX >> /dev/null
then
	srxip=$(grep -w "set security address-book global address" $SRX | awk '{ print $6 }')
else
	echo "Invalid IP or no rules found on SRX - exiting..."
	cd .. && cd ..
	rm -r .configaudit.$folder
	exit 0
fi
asaip=$(grep -w "network" $ASA | awk '{ print $3 }')

if [ "$(echo $srxip | sed 's:/.*::')" == "$asaip" ] 
then

	sed -n '/^G1-/,/^-G1/p;/^-G1/q' $SRX | sed -n '/G1-/,/-G1/{/G1-/!{/-G1/!p;};}' | sort > srxg
	sed -n '/^G1-/,/^-G1/p;/^-G1/q' $ASA | sed -n '/G1-/,/-G1/{/G1-/!{/-G1/!p;};}' | sort > asag
	sed -n '/^G2-/,/^-G2/p;/^-G2/q' $SRX | sed -n '/G2-/,/-G2/{/G2-/!{/-G2/!p;};}' | sort > srxpg
	sed -n '/^G2-/,/^-G2/p;/^-G2/q' $ASA | sed -n '/G2-/,/-G2/{/G2-/!{/-G2/!p;};}' | sort > asapg
	sed -n '/^G3-/,/^-G3/p;/^-G3/q' $SRX | sed -n '/G3-/,/-G3/{/G3-/!{/-G3/!p;};}' | sort > srxgpg
	sed -n '/^G3-/,/^-G3/p;/^-G3/q' $ASA | sed -n '/G3-/,/-G3/{/G3-/!{/-G3/!p;};}' | sort > asagpg
	sed -n '/^G4-/,/^-G4/p;/^-G4/q' $SRX | sed -n '/G4-/,/-G4/{/G4-/!{/-G4/!p;};}' | sort > srxggpg
	sed -n '/^G4-/,/^-G4/p;/^-G4/q' $ASA | sed -n '/G4-/,/-G4/{/G4-/!{/-G4/!p;};}' | sort > asaggpg

	srxgt=$(wc -l srxg | awk '{ print $1 }')
	asagt=$(wc -l asag | awk '{ print $1 }')
	gres=1
	i=1
	while [ $i -le $srxgt ] 
	do
		srxgn=$(sed "${i}q;d" srxg)
		asagn=$(sed "${i}q;d" asag)
		if [ "$srxgn" == "$asagn" ] 
		then
			i=$((i+1))
		else
			i=$((i+1))
			gres=0
		fi
	done

	srxpgt=$(wc -l srxpg | awk '{ print $1 }')
	asapgt=$(wc -l asapg | awk '{ print $1 }')
	pgres=1
	i=1
	while [ $i -le $srxpgt ] 
	do
		srxpgn=$(sed "${i}q;d" srxpg)
		asapgn=$(sed "${i}q;d" asapg)
		if [ "$srxpgn" == "$asapgn" ] 
		then
			i=$((i+1))
		else
			i=$((i+1))
			pgres=0
		fi
	done	

	srxgpgt=$(wc -l srxgpg | awk '{ print $1 }')
	asagpgt=$(wc -l asagpg | awk '{ print $1 }')
	gpgres=1
	i=1
	while [ $i -le $srxgpgt ] 
	do
		srxgpgn=$(sed "${i}q;d" srxgpg)
		asagpgn=$(sed "${i}q;d" asagpg)
		if [ "$srxgpgn" == "$asagpgn" ] 
		then
			i=$((i+1))
		else
			i=$((i+1))
			gpgres=0
		fi
	done

	srxggpgt=$(wc -l srxgpg | awk '{ print $1 }')
	asaggpgt=$(wc -l asagpg | awk '{ print $1 }')
	ggpgres=1
	i=1
	while [ $i -le $srxggpgt ] 
	do
		srxggpgn=$(sed "${i}q;d" srxggpg)
		asaggpgn=$(sed "${i}q;d" asaggpg)
		if [ "$srxggpgn" == "$asaggpgn" ] 
		then
			i=$((i+1))
		else
			i=$((i+1))
			ggpgres=0
		fi
	done

	echo "Group check" > result-audit
	echo "-----------" >> result-audit

	if [ $gres -eq 1 ]
	then
		echo "Groups: OK" >> result-audit
	else
		echo "Groups: Fail - $(diff --new-line-format="" --unchanged-line-format=""  srxg asag)" >> result-audit
	fi
	if [ $pgres -eq 1 ]
	then
		echo "PGroups: OK" >> result-audit
	else
		echo "PGroups: Fail - $(diff --new-line-format="" --unchanged-line-format=""  srxpg asapg)" >> result-audit
	fi
	if [ $gpgres -eq 1 ]
	then
		echo "GPGroups: OK" >> result-audit
	else
		echo "GPGroups: Fail - $(diff --new-line-format="" --unchanged-line-format=""  srxgpg asagpg)" >> result-audit
	fi
	if [ $ggpgres -eq 1 ]
	then
		echo "GGPGroups: OK" >> result-audit
	else
		echo "GGPGroups: Fail - $(diff --new-line-format="" --unchanged-line-format=""  srxgpg asaggpg)" >> result-audit
	fi
	echo "" >> result-audit

	echo "70%"

	sed -n '/^PS-/,/^-PS/p;/^-PS/q' $SRX > srxp
	sed -n '/^RS-/,/^-RS/p;/^-RS/q' $ASA > asap

	PStotal=$(grep ^PS $SRX | wc -l)
	PStotal=$((PStotal-1))
	touch srxconvertedrules
	for (( n=1; n<=$PStotal; n++))
	do
		echo "70% $n \ $PStotal"
		rule="access-list global_in extended permit"
		policy=$(grep -w "PS${n}" -w srxp | awk '{ print $2 }' | sed 's/.$//')
		awk "/$policy-/{flag=1;next}/-$policy/{flag=0}flag" srxp > srxr
		srcs=$(grep -w "source-address" srxr | wc -l) 
		dsts=$(grep -w "destination-address" srxr | wc -l)
		apps=$(grep -w "application" srxr | wc -l)
		for (( s=1; s<=$srcs; s++ ))
		do
			for (( a=1; a<=$apps; a++ ))
			do
				for (( d=1; d<=$dsts; d++ ))
				do
					obj0="object-group"
					obj1="any"
					obj2="any"
					app=$(grep -w "application" srxr | awk 'NF>1{print $NF}' | head -n $a | tail -n 1)
					case $app in
 						junos-http)
    						app="www"
    						obj0="tcp"
    						;;
    					junos-https)
    						app="https"
    						obj0="tcp"
    						;;
    					junos-smtp)
    						app="smtp"
    						obj0="tcp"
    						;;
    					junos-dns-udp)
    						app="53"
    						obj0="udp"
    						;;
    					junos-ntp)
    						app="ntp"
    						obj0="udp"
    						;;
    					junos-rsh)
    						app="rsh"
    						obj0="tcp"
    						;;
    					junos-syslog)
    						app="syslog"
    						obj0="udp"
    						;;
    					junos-ftp)
    						app="21"
    						obj0="tcp"
    						;;
    					junos-ssh)
    						app="ssh"
    						obj0="tcp"
    						;;
    					any)
    						app=""
    						obj0="ip"
    						;;
    					*)
							obj0="object-group"
						;;
					esac
					CIDR='(((25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?))(\/([8-9]|[1-2][0-9]|3[0-2]))([^0-9.]|$)'
					src=$(grep -w "source-address" srxr | awk 'NF>1{print $NF}' | head -n $s | tail -n 1)
					if echo $src | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" > /dev/null
					then
						obj1="object"
						src=$(echo $src | sed 's:/.*::')
					else
						if [[ $src =~ $CIDR ]]
						then
							obj1="object"
							src=$(echo $src | sed 's:/.*::')
						else
							obj1="object-group"
						fi
					fi
					dst=$(grep -w "destination-address" srxr | awk 'NF>1{print $NF}' | head -n $d | tail -n 1)
					if echo $dst | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" > /dev/null
					then
						obj2="object"
						dst=$(echo $dst | sed 's:/.*::')
					else
						if [[ $dst =~ $CIDR ]]
						then
							obj2="object"
							dst=$(echo $dst | sed 's:/.*::')
						else
							obj2="object-group"
						fi
					fi
					if [ "$obj0" == "object-group" ]
					then
						fullrule=$(echo $rule $obj0 $app $obj1 $src $obj2 $dst)
						if grep "$fullrule" srxconvertedrules >> /dev/null
						then
							echo $fullrule >> /dev/null
						else
							echo $fullrule >> srxconvertedrules
						fi
					else
						fullrule=$(echo $rule $obj0 $obj1 $src $obj2 $dst "eq" $app)
						if grep "$fullrule" srxconvertedrules >> /dev/null
						then
							echo $fullrule >> /dev/null
						else
							echo $fullrule >> srxconvertedrules
						fi
					fi
				done
			done
		done
	done	

	echo "80%"

	awk "/RS-/{flag=1;next}/-RS/{flag=0}flag" asap > asarules
	cat asarules | sort > asaruleslist
	cat srxconvertedrules | sort -u > srxruleslist

	echo "Rule check" >> result-audit
	echo "-----------" >> result-audit

	echo "90%"

	cat srxruleslist | while read srxrule
	do
		PGRP=$(echo $srxrule | awk '{ print $6 }')
		if echo $PGRP | grep "PGRP" >> /dev/null
		then
			PGRP="${PGRP:5}"
			case $PGRP in
    			''|*[!0-9]*)  ;;	
    			*) 
					ASRC=$(echo $srxrule | awk '{ print $8 }')
					ADST=$(echo $srxrule | awk '{ print $10 }')
					grep "$PGRP" asaruleslist | grep "$ASRC" | grep "$ADST" | while read -r asarule
					do
						echo $asarule >> asaremove
						echo $srxrule >> srxremove
					done
					;;
			esac
		fi
	done 
	echo "Rules present on SRX; missing on ASA:" >> result-audit
	if [ -f srxremove ]
	then
    	grep -Fvf srxremove srxruleslist | sort -r | uniq > srxresult
    else
    	cat srxruleslist | sort -r | uniq > srxresult
	fi
	if [ -f asaremove ]
	then
    	grep -Fvf asaremove asaruleslist | sort -r | uniq > asaresult
    else
    	cat asaruleslist | sort -r | uniq > asaresult
	fi
	
	diff --changed-group-format='%>' --unchanged-group-format='' srxresult asaresult > diffa
	diff --changed-group-format='%>' --unchanged-group-format='' asaresult srxresult > diffs
	cat diffs | sort -r | uniq > diffsrx
	cat diffa | sort -r | uniq > diffasa
	ida=1
	cat diffsrx | while read diff
	do
		echo $ida $diff >> result-audit
		ida=$((ida+1))
	done

	echo "" >> result-audit
	echo "Rules present on ASA; missing on SRX:" >> result-audit
	idb=1
	cat diffasa | while read diff
	do
		echo $idb $diff >> result-audit
		idb=$((idb+1))
	done
else
	echo "IPs don't match - exiting..."
	cd .. && cd ..
	rm -r .configaudit.$folder
	exit 0
fi

echo "100%"
cd .. && cd ..
mv ".configaudit.$folder/tmpaudit/result-audit" "./audit-result-$folder"
rm -r .configaudit.$folder